  fetch('https://jsonplaceholder.typicode.com/todos')
	.then(response => response.json())
	.then( json => console.log(json))


 fetch('https://jsonplaceholder.typicode.com/todos')
	.then(res => res.json())
	.then(data => {
		console.log(data.map(({title}) => title))
	})


fetch('https://jsonplaceholder.typicode.com/todos1')
	.then( res => res.json())
	.then( data => console.log(`The item" ${data.title}"on the list has a status of ${data.completed}`))


fetch("https://jsonplaceholder.typicode.com/todos", {
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},
	body: JSON.stringify({
		userID: 9,
		title: "tasdifjsdf",
		completed: false
	})
})
	.then(response => response.json())
	.then(newTodo => console.log(newTodo))



fetch('https://jsonplaceholder.typicode.com/posts/9', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
	title: "The Jaw",
	description: "Horror",
	status: true,
	dateCompleted: "2011-03-22",
	userId: 9
	})
})
	.then(res => res.json())
	.then(data => console.log(data))